package course.examples.examenpractico1;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    private Button dias;
    private EditText num;
    private TextView dia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dias=(Button)findViewById(R.id.button);
        dias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                num = (EditText) findViewById(R.id.numdia);
                dia = (TextView) findViewById(R.id.textView);
                try {
                    float nd=Float.parseFloat(num.getText().toString());
                    if (nd == 1) {dia.setText("El dia es Domingo");}
                    if (nd == 2){ dia.setText("El dia es Lunes");}
                    if (nd == 3){ dia.setText("El dia es Martes");}
                    if (nd == 4) {dia.setText("El dia es Miercoles");}
                    if (nd == 5) {dia.setText("El dia es Jueves");}
                    if (nd == 6){ dia.setText("El dia es Viernes");}
                    if (nd == 7){ dia.setText("El dia es Sabado");}
                    if (nd <1 || nd >7){
                        dia.setText("el numero ingresado no corresponde a ningun dia");
                    }

                }

                catch( Exception e){
                    dia.setText("Ingrese solo numeros");
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
